package com.example.geotagginapp.utils;

/**
 * Created by pravina on 26/6/18.
 */

public class AppConstants {
       public static final String NO_NETWORK = "NO NETWORK";

    public static final String NETWORK_TIMEOUT_ERROR = "Network Timeout Error";

    public static final String ERROR_INVALID_CREDENTIALS_RES =
            "invalid credentials";
    public static final String ERROR_INVALID_TOKEN_RES = "invalidToken";
    public static final String ERROR_GENERIC_ERROR = "Generic Error";

    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final String CONTENT_TYPE__URLENCODED ="application/x-www-form-urlencoded";


    public static final String INFORMATION = "INFORMATION";
    public static final int REQUEST_CAMERA = 120;
    public static final int REQUEST_LOCATION = 130;
}
