package com.example.geotagginapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

public class SettingPreference {

    public static final String PREFS_NAME = "mySettings";

    public static void setStringValueInPref(Context context, List<String> prefKey,
                                            int actualValue) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE).edit();
       /* Set<String> set = new HashSet<String>();
        set.addAll(prefKey);
        editor.putStringSet("key", set);*/

        for(int i = 0; i<prefKey.size(); i++) {
            editor.putString("position" + i, prefKey.get(i));
        }

        editor.commit();

    }


}
