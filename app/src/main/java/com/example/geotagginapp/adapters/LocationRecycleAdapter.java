package com.example.geotagginapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.geotagginapp.R;
import com.example.geotagginapp.interfaces.OnClickItemListner;
import com.example.geotagginapp.objects.Result;
import com.example.geotagginapp.objects.Spacecraft;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


public class LocationRecycleAdapter extends RecyclerView.Adapter<LocationRecycleAdapter.RecyclerViewHolders> {

    private List<Spacecraft> itemList;
    private Context context;
    String packageName;
    Fragment fragment;
    List<Integer> postionArray = new ArrayList<>();
    OnClickItemListner onClickItemListner;


    public LocationRecycleAdapter(Context context, ArrayList<Spacecraft> itemList, Fragment fragment, OnClickItemListner onClickItemListener) {
        this.itemList = itemList;
        this.context = context;
        this.fragment = fragment;
        this.onClickItemListner = onClickItemListener;
        packageName = context.getPackageName();
    }


    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_card, null);

        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {

        final Spacecraft s=itemList.get(position);

        holder.txtMovieName.setText(s.getName());
        Picasso.get().load(s.getUri()).placeholder(R.drawable.placeholder).into(holder.imageView);


        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onClickItemListner.onItemClick(s.getName());
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView txtMovieName;
        private RelativeLayout rlMain;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            txtMovieName = (TextView) itemView.findViewById(R.id.txtMovieName);
            rlMain=(RelativeLayout)itemView.findViewById(R.id.rlMain);






        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}