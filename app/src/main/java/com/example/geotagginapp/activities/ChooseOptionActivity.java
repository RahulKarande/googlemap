package com.example.geotagginapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.geotagginapp.MapsActivity;
import com.example.geotagginapp.R;
import com.example.geotagginapp.baseactivity.MasterActivity;
import com.example.geotagginapp.baseactivity.MasterFragment;

public class ChooseOptionActivity extends MasterActivity implements View.OnClickListener {

    Button btnMap;
    Button btnChose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_option);
    btnMap =(Button)findViewById(R.id.btnMap);
    btnChose=(Button)findViewById(R.id.btnList);

    btnMap.setOnClickListener(this);
    btnChose.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnMap:
                Intent intent =new Intent(ChooseOptionActivity.this, MapsActivity.class);
                startActivity(intent);
                break;

            case R.id.btnList:
                Intent loadintent =new Intent(ChooseOptionActivity.this, MainActivity.class);
                startActivity(loadintent);
                break;
        }
    }
}
