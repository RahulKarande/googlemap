package com.example.geotagginapp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.geotagginapp.MapsActivity;
import com.example.geotagginapp.R;
import com.example.geotagginapp.adapters.LocationRecycleAdapter;
import com.example.geotagginapp.baseactivity.MasterFragment;
import com.example.geotagginapp.interfaces.OnClickItemListner;
import com.example.geotagginapp.objects.Spacecraft;
import com.example.geotagginapp.utils.AppConstants;
import com.example.geotagginapp.utils.SimpleDividerItemDecoration;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class LocationListFragment extends MasterFragment implements OnClickItemListner, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView recyclerView;
    private String mParam1;
    private String mParam2;
    View view;
    private LinearLayoutManager lLayout;
    LocationRecycleAdapter adapter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_test, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Location");


        initView();

    }

    private void initView() {
        try {
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
            lLayout = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(lLayout);
            adapter = new LocationRecycleAdapter(getActivity(), getData(), LocationListFragment.this, LocationListFragment.this);
            adapter.notifyDataSetChanged();
            recyclerView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private ArrayList<Spacecraft> getData()
    {
        ArrayList<Spacecraft> spacecrafts=new ArrayList<>();

        File downloadsFolder = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getApplicationContext().getPackageName()
                + "/Files");

        Spacecraft s;

        if(downloadsFolder.exists())
        {
            //GET ALL FILES IN DOWNLOAD FOLDER
            File[] files=downloadsFolder.listFiles();

            //LOOP THRU THOSE FILES GETTING NAME AND URI
            for (int i=0;i<files.length;i++)
            {
                File file=files[i];

                s=new Spacecraft();
                s.setName(file.getName());
                s.setUri(Uri.fromFile(file));

                spacecrafts.add(s);
            }
        }


        return spacecrafts;
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(String name) {

        Intent intent =new Intent(getActivity(), MapsActivity.class);
        intent.putExtra(AppConstants.INFORMATION,name);
        startActivity(intent);

    }
}
